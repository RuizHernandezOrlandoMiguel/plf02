(ns plf02.clj)
;####################################################################################################################
;                                        FUNCIONES DE ORDEN SUPERIOR
;####################################################################################################################

(defn funcion-drop-1
  [x y] (drop x y))

(defn funcion-drop-2
  [x y] (drop x y))

(defn funcion-drop-3
  [x y] (drop x y))

(funcion-drop-1 3 [75 1 9 4 2 9 1])
(funcion-drop-2 4 '(5 3 1 7 9 2 5 2 1))
(funcion-drop-3 2 #{\a \b \c \d \e})




(defn funcion-drop-last-1
  [x] (drop-last x))

(defn funcion-drop-last-2
  [x y] (drop-last x y))

(defn funcion-drop-last-3
  [x y] (drop-last x y))

(funcion-drop-last-1 [12 3 4 5 6])
(funcion-drop-last-2 2 '("uno" "dos" "tres" "cuatro" "cinco" "seis"))
(funcion-drop-last-3 2 [1 3 5 6 2 3])




(defn funcion-dropWhile-1
  [x] (drop-while neg? x))
(funcion-dropWhile-1 [-1 1 1 1 2 3 4])

(defn funcion-dropWhile-2
  [x] (drop-while pos? x))
(funcion-dropWhile-2 [1 1 1 1 -2 -3 -4])

(defn funcion-dropWhile-3
  [x] (drop-while char? x))
(funcion-dropWhile-3 #{\a \b \c 2 4})




(defn funcion-every?-1
  [x] (every? even? x))

(defn funcion-every?-2
  [x] (every? even? x))

(defn funcion-every?-3
  [x] (every? even? x))

(funcion-every?-1 '(2 4 6))
(funcion-every?-2 '(1 3 5))
(funcion-every?-3 #{2 4 6 8 10})






(defn funcion-filterv-1
  [x] (filterv even? x))

(defn funcion-filterv-2
  [x] (filterv even? x))

(defn funcion-filterv-3
  [x] (filterv even? x))

(funcion-filterv-1 (range 10))
(funcion-filterv-2 '(1 2 3 4 5 6 7 8))
(funcion-filterv-3 #{10 20 2 13 33 7})





(defn funcion-groupBy-1
  [x] (group-by odd? x))

(defn funcion-groupBy-2
  [x] (group-by odd? x))

(defn funcion-groupBy-3
  [x] (group-by odd? x))

(funcion-groupBy-1 (range 10))
(funcion-groupBy-2 '(1 2 5 6 7 8 10 13 262 33))
(funcion-groupBy-3 #{1 2 5 6 7 8 10 13 262 33})




(defn funcion-iterate-1
  [x] (take 5 (iterate x 5)))

(defn funcion-iterate-2
  [x] (take 5 (iterate x 5)))

(defn funcion-iterate-3
  [x] (take 10 (iterate x 5)))

(funcion-iterate-1 inc)
(funcion-iterate-2 dec)
(funcion-iterate-3 inc)





(defn funcion-keep-1
  [x] (keep #(if (pos? %) %) x))

(defn funcion-keep-2
  [x] (keep #(if (neg? %) %) x))

(defn funcion-keep-3
  [x] (keep #(if (pos? %) %) x))

(funcion-keep-1 '(-1 -3 1 2 3))
(funcion-keep-2 [-1 -3 1 2 3])
(funcion-keep-3 (range 10))




(defn funcion-keepIndexed-1
  [x] (keep-indexed #(if (odd? %1) %2) x))

(defn funcion-keepIndexed-2
  [x] (keep-indexed #(if (odd? %1) %2) x))

(defn funcion-keepIndexed-3
  [x] (keep-indexed #(if (odd? %1) %2) x))

(funcion-keepIndexed-1 [:a :b :c :d :e :f])
(funcion-keepIndexed-2 '(1 2 3 4 5 6))
(funcion-keepIndexed-3 #{\a \b \c \d \e \f})




(defn funcion-mapIndexed-1
  [x] (map-indexed vector x))

(defn funcion-mapIndexed-2
  [x] (map-indexed vector x))

(defn funcion-mapIndexed-3
  [x] (map-indexed vector x))

(funcion-mapIndexed-1 "map-indexed")
(funcion-mapIndexed-2 #{\a \b \c \d \e})
(funcion-mapIndexed-3 '(6 52 4 1 4 0))





(defn funcion-mapcat-1
  [x] (mapcat  #(remove even? %) x))
(funcion-mapcat-1 [[1 2] [2 2] [2 3]])


(defn funcion-mapcat-1
  [x] (mapcat  reverse x))

(defn funcion-mapcat-2
  [x] (mapcat  reverse x))

(defn funcion-mapcat-3
  [x] (mapcat  reverse x))

(funcion-mapcat-1 [[3 2 1 0] [6 5 4] [9 8 7]])
(funcion-mapcat-2 '([3 2 1 0] [6 5 4] [9 8 7]))
(funcion-mapcat-3 '([9 8 7] [4 2 1] [5 6 7]))





(defn funcion-mapv-1
  [x] (mapv + [1 2 3] x))
(funcion-mapv-1 [4 5 6])

(defn funcion-mapv-2
  [x] (mapv + [1 2 3] x))
(funcion-mapv-2 '(4 5 6))

(defn funcion-mapv-3
  [x] (mapv + [1 2 3] x))
(funcion-mapv-3 (range 3))






(defn funcion-merge-with-1
  [x y] (merge-with + x y))

(defn funcion-merge-with-2
  [x y] (merge-with + x y))

(defn funcion-merge-with-3
  [x y] (merge-with into x y))

(funcion-merge-with-1 {:a 1 :b 3 :c 4} {:a 4 :b 2 :c 5})
(funcion-merge-with-2 {:a 1 :b 3 :c 4} {:a -4 :b -2 :c -5})
(funcion-merge-with-3 {"uno" ["a" "b" "c"] "dos" ["d" "e" "f"]}
                      {"uno" ["1" "2" "3"] "dos" ["4" "5" "6"]})




(defn funcion-no-any?-1
  [x] (not-any? odd? x))

(defn funcion-no-any?-2
  [x] (not-any? odd? x))

(defn funcion-no-any?-3
  [x] (not-any? odd? x))

(funcion-no-any?-1 '(4 2 6))
(funcion-no-any?-2 (range 10))
(funcion-no-any?-3 [2 4 6])




(defn funcion-not-every?-1
  [x] (not-every? odd? x))

(defn funcion-not-every?-2
  [x] (not-every? odd? x))

(defn funcion-not-every?-3
  [x] (not-every? odd? x))

(funcion-not-every?-1 '(2 4 1 8))
(funcion-not-every?-2 [1 3 5])
(funcion-not-every?-3 (range 10))




(defn funcion-parttition-by-1
  [x y]
  (partition-by y x))

(defn funcion-parttition-by-2
  [x y]
  (partition-by y x))

(defn funcion-parttition-by-3
  [x y]
  (partition-by y x))

(funcion-parttition-by-1 [1 2 3 4 5 6 7 8 9] #(= 2 %))
(funcion-parttition-by-2 (range 10) #(= 4 %))
(funcion-parttition-by-3 '(1 2 3 4 5) #(= 3 %))





(defn funcion-reduce-kv-1 [x]
  (reduce-kv (fn [s k v] (if (even? k) (+ s v) s)) 0 x))

(defn funcion-reduce-kv-2 [x]
  (reduce-kv (fn [s k v] (if (even? k) (+ s v) s)) 0 x))

(defn funcion-reduce-kv-3 [x]
  (reduce-kv (fn [s k v] (if (even? k) (+ s v) s)) 0 x))

(funcion-reduce-kv-1 (vec (range 10)))
(funcion-reduce-kv-2 [0 1 2 3 4 5 6 7 8 9])
(funcion-reduce-kv-3 [-1 -2 -3 -4 -5 6 7 8 9])




(defn funcion-remove-1
  [x y] (remove x y))

(defn funcion-remove-2
  [x y] (remove x y))

(defn funcion-remove-3
  [x y] (remove x y))

(funcion-remove-1 pos? [1 -2 2 -1 3 7 0])
(funcion-remove-2 neg? [1 -2 2 -1 3 7 0])
(funcion-remove-3 pos? '(1 -2 2 -1 3 7 0))





(defn funcion-reverse-1
  [x] (reverse x))

(defn funcion-reverse-2
  [x] (reverse x))

(defn funcion-reverse-3
  [x] (reverse x))

(funcion-reverse-1 [1 2 3 4 5 6])
(funcion-reverse-1 '(1 2 3 4 5 6))
(funcion-reverse-1 (range 6))


(some even? '(1 2 3 4))

(defn funcion-some-1
  [x] (some even? x))

(defn funcion-some-2
  [x] (some even? x))

(defn funcion-some-3
  [x] (some even? x))

(funcion-some-1 '(1 2 3 4))
(funcion-some-2 [1 2 3 4])
(funcion-some-3 (range 4))



(defn funcion-short-by-1
  [x] (sort-by count x))

(defn funcion-short-by-2
  [x] (sort-by count x))

(defn funcion-short-by-3
  [x] (sort-by count x))

(funcion-short-by-1 ["aaa" "bb" "c"])
(funcion-short-by-3 #{"aaa" "bb" "c"})
(funcion-short-by-2 '("aaa" "bb" "c"))




(defn funcion-split-with-1
  [x] (split-with (partial >= 3) x))

(defn funcion-split-with-2
  [x] (split-with (partial >= 3) x))

(defn funcion-split-with-3
  [x] (split-with (partial >= 3) x))

(funcion-split-with-1 [1 2 3 4 5])
(funcion-split-with-1 '(1 2 3 4 5))
(funcion-split-with-1 (range 6))



(defn funcion-take-1
  [x y] (take x y))

(defn funcion-take-2
  [x y] (take x y))

(defn funcion-take-3
  [x y] (take x y))

(funcion-take-1 3 '(1 2 3 4 5 6))
(funcion-take-2 5 [1 2 3 4 5 6])
(funcion-take-3 4 (range 10))





(defn funcion-take-last-1
  [x y] (take-last x y))

(defn funcion-take-last-2
  [x y] (take-last x y))

(defn funcion-take-last-3
  [x y] (take-last x y))

(funcion-take-last-1 3 '(1 2 3 4 5 6))
(funcion-take-last-2 5 [1 2 3 4 5 6])
(funcion-take-last-3 4 (range 10))





(defn funcion-take-nth-1
  [x y] (take-nth x y))

(defn funcion-take-nth-2
  [x y] (take-nth x y))

(defn funcion-take-nth-3
  [x y] (take-nth x y))

(funcion-take-nth-1 3 '(0 1 2 3 4 5 6))
(funcion-take-nth-2 2 [0 1 2 3 4 5 6])
(funcion-take-nth-3 3 (range 30))



(defn funcion-take-while-1
  [x y] (take-while x y))

(defn funcion-take-while-2
  [x y] (take-while x y))

(defn funcion-take-while-3
  [x y] (take-while x y))

(funcion-take-while-1 neg? [-2 -1 0 1 2 3])
(funcion-take-while-1 pos? '(1 1 1 3 4 -2 -1 0 1 2 3))
(funcion-take-while-1 #(> 3 %) [-2 -1 0 1 2 3 4])





(defn funcion-update-1
  [x y z] (update x y z))

(defn funcion-update-2
  [x y z] (update x y z))

(defn funcion-update-3
  [x y z] (update x y z))

(funcion-update-1 {:name "James" :age 26} :age inc)
(funcion-update-1 {:name "James" :age 26} :age dec)
(funcion-update-1 {:name "Jaime" :age 30} :age inc)




(defn funcion-update-in-1
  [x y z] (update-in x y z))

(defn funcion-update-in-2
  [x y z] (update-in x y z))

(defn funcion-update-in-3
  [x y z] (update-in x y z))

(funcion-update-in-1 [{:name "James" :age 26}  {:name "John" :age 43}] [1 :age] inc)
(funcion-update-in-2 [{:name "James" :age 26}  {:name "John" :age 43}] [0 :age] dec)
(funcion-update-in-3 [{:name "James" :age 26}  {:name "John" :age 43} {:name "Peter" :age 18}] [2 :age] inc)


;####################################################################################################################
;                                                   PREDICADO
;####################################################################################################################

(defn funcion-asosiative?-1
  [x] (associative? x))

(defn funcion-asosiative?-2
  [x] (associative? x))

(defn funcion-asosiative?-3
  [x] (associative? x))

(funcion-asosiative?-1 [1 2 3])
(funcion-asosiative?-2 '(1 2 3))
(funcion-asosiative?-3 {:a 1 :b 2})




(defn funcion-boolean?-1
  [x] (boolean? x))

(defn funcion-boolean?-2
  [x] (boolean? x))

(defn funcion-boolean?-3
  [x] (boolean? x))

(funcion-boolean?-1 0)
(funcion-boolean?-2 false)
(funcion-boolean?-3 true)




(defn funcion-char?-1
  [x] (char? x))

(defn funcion-char?-2
  [x] (char? x))

(defn funcion-char?-3
  [x] (char? x))

(funcion-char?-1 \a)
(funcion-char?-2 #{\a})
(funcion-char?-3 0)





(defn función-coll?-1
  [x]
  (coll? x))

(defn función-coll?-2
  [x y]
  (coll? (+ x y)))

(defn función-coll?-3
  [x]
  (coll? x))

(función-coll?-1 [1 2 3])
(función-coll?-2 3 4)
(función-coll?-3 '("columna 1" "columna2"))




(defn función-decimal?-1
  [x y]
  (decimal? (+ x y)))

(defn función-decimal?-2
  [x]
  (decimal? x))

(defn función-decimal?-3
  [x]
  (decimal? x))

(función-decimal?-1 1M  3)
(función-decimal?-2 "hola")
(función-decimal?-3 [1 2 3 4])




(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a b]
  (double? (- a b)))

(defn función-double?-3
  [a]
  (double? (first a)))


(función-double?-1 1.0)
(función-double?-2 5.5 3)
(función-double?-3 [2.0 5.0 1.0])




(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a b]
  (float? (* a b)))

(defn función-float?-3
  [a b]
  (float? (/ a b)))

(función-float?-1 1.0)
(función-float?-2 3 2.1)
(función-float?-3 4 5)





(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a b]
  (ident? (conj a b)))

(función-ident?-1 'abc)
(función-ident?-1 :a)
(función-ident?-3 [1 2 3] :b)





(defn función-indexed?-1
  [a]
  (indexed? a))

(defn función-indexed?-2
  [a b]
  (indexed? (a b)))

(defn función-indexed?-3
  [a]
  (indexed? a))

(función-indexed?-1 [1 2 3])
["hola" "jojo"] 1
(función-indexed?-2 ["hola" "jojo"] 1)
(función-indexed?-3 [1 2 3 {:a 1 :b 2}])





(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [a b]
  (int? (+ a b)))

(defn función-int?-3
  [a b]
  (int? (/ a b)))

(función-int?-1 25.0)
(función-int?-2 4 9)
(función-int?-3 1 4)






(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a b c]
  (integer? (+ a b c)))

(defn función-integer?-3
  [a]
  (integer? (count a)))

(función-integer?-1 10)
(función-integer?-2 3 6 8)
(función-integer?-3 [\A \B \C])





(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? (key (apply max-key a))))


(función-keyword?-1 :a.b.c)
(función-keyword?-2 {:a 1 :b 2 :c 10})
(función-keyword?-3 {:a 10 :b 20})





(defn función-list?-1
  [a]
  (list? a))

(defn función-list?-2
  [a]
  (list? (range a)))

(defn función-list?-3
  [a b]
  (list? (conj a b)))

(función-list?-1 '(1 2 3 4 5 6))
(función-list?-2 10)
'("hola" "adios")
(función-list?-3 '("hola" "adios") '(1 2 3 4))




(defn función-map-entry?-1
  [a]
  (map-entry? (first a)))

(defn función-map-entry?-2
  [a]
  (map-entry? a))

(defn función-map-entry?-3
  [a]
  (map-entry? (key (apply max-key a))))

(función-map-entry?-1 {:a 10 :b 20 :c 30})
(función-map-entry?-2 {:a 4 :c 10})
(función-map-entry?-3 {:a 10 :b 50 :c 70})





(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a b]
  (map? (conj a b)))

(defn función-map?-3
  [a]
  (map? (vector a)))


(función-map?-1 {:a "hola" :b "20"})
{:c 40 :d 120} {:a 10 :b 100}
(función-map?-2 {:c 40 :d 120} {:a 10 :b 100})
(función-map?-3 "hola")




(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a b]
  (nat-int? (+ a b)))

(defn función-nat-int?-3
  [a b]
  (nat-int? (/ a b)))

(función-nat-int?-1 999)
(función-nat-int?-2 999 -1000)
(función-nat-int?-3 50 10)




(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? (first [a])))

(defn función-number?-3
  [a]
  (number? (first (map val a))))




(función-number?-1 10)
(función-number?-2 [true false true false])
(función-number?-3 {:a 10 :b 100 :c 200})

(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a b]
  (pos-int? (+ a b)))

(defn función-pos-int?-3
  [a]
  (pos-int? (first a)))

(función-pos-int?-1 21345)
(función-pos-int?-2 50 -100)
(función-pos-int?-3 [10 true 20 false])





(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [a b]
  (ratio? (- a b)))

(defn función-ratio?-3
  [a b c d]
  (ratio? (max a b c d)))


(función-ratio?-1 5/4)
(función-ratio?-2 1/2 1/4)
(función-ratio?-3 1 3 4 21/4)






(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a b]
  (rational? (+ a b)))

(defn función-rational?-3
  [a b c]
  (rational? (* a b c)))

(función-rational?-1 10)
(función-rational?-2 2/1 3)
(función-rational?-3 0.10 0.5 0.5)






(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a]
  (seq? (range a 5)))

(defn función-seq?-3
  [a]
  (seq? (seq a)))

(función-seq?-1 '(1 2 3))
(función-seq?-2 10)
(función-seq?-3 [true false true])





(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a]
  (fn [b] (seqable? (conj a b))))

(defn función-seqable?-3
  [a]
  (seqable? (first [a])))

(función-seqable?-1 [])
((función-seqable?-2 {}) {})
(función-seqable?-3 [nil true false])






(defn función-sequential?-1
  [a]
  (sequential? a))

(defn función-sequential?-2
  [a]
  (sequential? (range a)))

(defn función-sequential?-3
  [a b]
  (sequential? (a b)))




(función-sequential?-1 [1 2 3 4 5 6])
(función-sequential?-2 16)
(función-sequential?-3 #{1 2 3 4 5} #{6 7 8 9})

(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a b c]
  (set? (hash-set a b c)))

(defn función-set?-3
  [a b]
  (set?  (conj a b)))

(función-set?-1 #{1 2 3 4})
(función-set?-2 1 2 3)
(función-set?-3 #{1 2 3 4} #{5 6 7 8})






(defn función-some?-1
  [a]
  (some? a))


(defn función-some?-2
  [a b]
  (some? (+ a b)))

(defn función-some?-3
  [a]
  (some? a))

(función-some?-1 45)
(función-some?-2 0 0)
(función-some?-3 [])






(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? (count [a])))

(defn función-string?-3
  [a]
  (string? (first a)))

(función-string?-1 "hola")
(función-string?-2 ["Alexis" "Morales" "Martinez"])
(función-string?-3 '("plf01" "plf02"))






(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? (first a)))

(defn función-symbol?-3
  [a]
  (symbol? (range a)))

(función-symbol?-1 'z)
(función-symbol?-2 ['a 'b 'c 'd])
(función-symbol?-3 9)



(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
  (vector? a))

(función-vector?-1 ["hola" "buenas noches"])
(función-vector?-2 (vector 1 2 3))
(función-vector?-2 '(1 2 3 4 5 6 7))